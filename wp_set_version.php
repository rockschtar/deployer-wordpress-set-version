<?php
namespace Deployer;

desc('Set Version for WordPress Projects');

task('wordpress:set_version', function() {
    $default_version = get('branch');
    $options = ['tag', 'branch', 'revision'];
    $version = false;

    foreach($options as $option) {
        if (input()->hasOption($option)) {
            $option_value = input()->getOption($option);
            if(!empty($option_value)) {
                $version = $option_value;
                break;
            }
        }
    }

    $version = $version === false ? $default_version : $version;

    $update_version_header = function($version, $file) {
        $sed_version = str_replace('/', '\/', $version);
        echo '➤ Update version ' . $version . ' in file ' . $file . "\n";
        run('sed -i "s/Version:.*$/Version: ' . $sed_version . '/" ' . $file);
    };

    $files = get('set_version_files', []);

    foreach($files as $file) {
        $update_version_header($version, get('current_path') . DIRECTORY_SEPARATOR . $file);
    }
});
